package ClassesAndObjects;

import java.io.*;

public class Console {
    public static void readChar() {
        int x;
        System.out.print("Please, write a symbol: ");
        try {
            x = System.in.read();
            char c = (char) x;
            System.out.println("Symbol code: " + c + " = " + x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void readName() {
        // Byte stream System.in transmitted to the stream reader constructor when creating the object of the class InputStreamReader
        InputStreamReader is = new InputStreamReader(System.in);

        // Performing buffering of the data that exclude necessary to call the data source when performing reading
        BufferedReader bis = new BufferedReader(is);

        try {
            System.out.print("Please, enter your name and press <Enter>: ");
            String name = bis.readLine();
            System.out.println("Hello, " + name);
        } catch (IOException e) {
            System.err.println("Input error " + e);
        }
    }
}
