package ClassesAndObjects;

//It also can be final and abstract
//TODO: read about friendly spec
public class User {
    public int numericCode; //here is an encapsulation violation!
    private String password;

    public void setNumericCode(int value) {
        if (value > 0) numericCode = value;
        else  numericCode = 1;
    }

    public int getNumericCode() {
        return numericCode;
    }

    public void setPassword(String pass) {
        if (pass != null) password = pass;
        else password = "11111";
    }

    public String getPassword() {
        return password;
    }
}
