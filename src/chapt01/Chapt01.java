package chapt01;
import java.util.Scanner;

public class Chapt01 {
    public void scanAndOutput() {
        System.out.println("Please, enter 2 numbers between 1 sand 10");
        Scanner scan = new Scanner(System.in);
        System.out.print("Number one = ");
        int number1 = scan.nextInt();
        System.out.print("Number two = ");
        int number2 = scan.nextInt();
        System.out.println("Cool! Here are your numbers: " + number1 + ", " + number2);
    }
}
