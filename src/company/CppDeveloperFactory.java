package company;

/**
 * The type Cpp developer factory.
 */
public class CppDeveloperFactory implements DeveloperFactory {
    @Override
    public Developer createDeveloper(String name, double salary) {
        return new CppDeveloper(name, salary);
    }
    @Override
    public Developer createDeveloper(String name, double salary, String nickName) {
        return new CppDeveloper(name, salary, nickName);
    }
}
