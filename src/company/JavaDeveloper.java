package company;

/**
 * The type Java developer.
 */
public class JavaDeveloper extends Developer {
    /**
     * Instantiates a new Java developer.
     *
     * @param name   the name
     * @param string the string
     */
    public JavaDeveloper(String name, double string) {
        super(name, string);
    }

    /**
     * Instantiates a new Java developer.
     *
     * @param name     the name
     * @param string   the string
     * @param nickName the nick name
     */
    public JavaDeveloper(String name, double string, String nickName) {
        super(name, string, nickName);
    }
    public void whoAmI() {
        System.out.println("I'm a Java developer");
    }
}
