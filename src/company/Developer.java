package company;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Random;

/**
 * The type Developer.
 */
public class Developer {
    final private String name;
    private String nickName = null;
    final private LocalDate hiringDate;
    private double salary;
    /**
     * The constant nextId.
     */
    public static int nextId;

    // The initialize block for statics fields. This block is performed when class is loaded once
    static {
        var generator = new Random();
        nextId = generator.nextInt(10000);
    }

    // This initialize block is performed each time as a new class object is created
    {
        this.hiringDate = LocalDate.now();
    }

    /**
     * Instantiates a new Developer.
     *
     * @param name   the name
     * @param salary the salary
     */
    public Developer(String name, double salary) {
        this.name = Objects.requireNonNullElse(name, "unknown"); //TODO: make sure, what is "this"?
        this.salary = salary;
    }

    /**
     * Instantiates a new Developer.
     *
     * @param name     the name
     * @param salary   the salary
     * @param nickName the nick name
     */
    public Developer(String name, double salary, String nickName) { // Method overload
        this(name, salary); // call the constructor above
        this.nickName = nickName;
    }

    /**
     * Sets nick name.
     *
     * @param nickName the nick name
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    /**
     * Gets nick name.
     *
     * @return the nick name
     */
    public String getNickName() {
        return this.nickName;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets hiring date.
     *
     * @return the hiring date
     */
    public LocalDate getHiringDate() {
        return hiringDate;
    }

    /**
     * Gets salary.
     *
     * @return the salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Raise salary double.
     *
     * @param byPercent the by percent
     * @return the double
     */
    public double raiseSalary(int byPercent) {
        double raise = this.salary * byPercent / 100;
        return this.salary += raise;
    }

    /**
     * Who am i.
     */
    public void whoAmI() {
        System.out.println("I'm just a developer!");
    }
}
