package company;

/**
 * The type Java developer factory.
 */
public class JavaDeveloperFactory implements DeveloperFactory{
    @Override //TODO: make sure, what is the @Override?
    public Developer createDeveloper(String name, double salary) {
        return new JavaDeveloper(name, salary);
    }
    @Override
    public Developer createDeveloper(String name, double salary, String nickName) {
        return new JavaDeveloper(name, salary, nickName);
    }
}
