package company;

/**
 * The type Cpp developer.
 */
public class CppDeveloper extends Developer {
    /**
     * Instantiates a new Cpp developer.
     *
     * @param name   the name
     * @param string the string
     */
    public CppDeveloper (String name, double string) {
        super(name, string); // Call the constructor of the parent class
    }

    /**
     * Instantiates a new Cpp developer.
     *
     * @param name     the name
     * @param string   the string
     * @param nickName the nick name
     */
    public CppDeveloper(String name, double string, String nickName) {
        super(name, string, nickName);
    }
    public void whoAmI() {
        System.out.println("I'm a C++ developer");
    }
}
