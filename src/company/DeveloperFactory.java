package company;

/**
 * The interface Developer factory.
 */
public interface DeveloperFactory {
    /**
     * Create developer developer.
     *
     * @param name   the name
     * @param salary the salary
     * @return the developer
     */
    Developer createDeveloper(String name, double salary);

    /**
     * Create developer developer.
     *
     * @param name     the name
     * @param salary   the salary
     * @param nickName the nick name
     * @return the developer
     */
    Developer createDeveloper(String name, double salary, String nickName);
}
