package company;

import java.util.Random;

/**
 * The type Program.
 */
public class Program {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        var developers = generateDevelopers();

        changeField(developers[0]);

        for (Developer developer: developers) {
            System.out.print(
                    "My name is " + developer.getName() + ". My nickname is " + developer.getNickName() +
                            ". "  + "My salary is " + developer.getSalary() + ". My id is " + Developer.nextId + ". "
            );
            developer.whoAmI();
        }
    }

    private static void changeField(Developer d) {
        d.setNickName("random name"); // Here we change nickname only for the passed user
    }


    private static DeveloperFactory getDeveloperFactoryBySpeciality(String speciality) {
        return switch (speciality) {
            case "Java" -> new JavaDeveloperFactory();
            case "C++" -> new CppDeveloperFactory();
            default -> throw new RuntimeException(speciality + " is unknown speciality.");
        };
    }

    private static Developer[] generateDevelopers() {
        String[] names = {"Josef", "Ivan", "Nikolai", "Stefan", "Georg"};
        String[] specialities = {"Java", "C++"};
        Double[] salaries = {1000d, 1500d, 2000d, 2000d, 2000d};

        var developers = new Developer[5];
        for (int i = 0; i < 5; i++) {
            int rnd = new Random().nextInt(specialities.length);
            DeveloperFactory developerFactory = getDeveloperFactoryBySpeciality(specialities[rnd]);
            if (i == 1) {
                developers[i] = developerFactory.createDeveloper(names[i], salaries[i], "Custom nickname"); // Method overload
            } else {
                developers[i] = developerFactory.createDeveloper(names[i], salaries[i]); // Method overload
            }
        }

        return developers;
    }
}
