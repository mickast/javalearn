package job;

import java.time.LocalDate;

public class Employee {
    final private String name;
    private double salary;
    final private LocalDate hireDay;

    public Employee(String n, double s, int year, int month, int day) {
        name = n;
        salary = s;
        hireDay = LocalDate.of(year, month, day);
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    public LocalDate getHireDay() {
        return hireDay;
    }

    public void changeSalary(double newSalary) {
        salary = newSalary;
    }
}
