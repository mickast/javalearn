package com.mikhail.javalearn.nestedClasses;

import org.jetbrains.annotations.NotNull;

public class ArrayAlg {
    public static Pair minmax(double @NotNull [] values){
        var min = Double.MIN_VALUE;
        var max = Double.MAX_VALUE;

        for (double v : values) {
            if (min > v) min = v;
            if (max < v) max = v;
        }

        return new Pair(min, max);
    }

    public static class Pair {
        private final double max;
        private final double min;

        public Pair(double min, double max) {
            this.min = min;
            this.max = max;
        }

        public double getMin() {
            return min;
        }

        public double getMax() {
            return max;
        }
    }

}
