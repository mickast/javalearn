package com.mikhail.javalearn.nestedClasses;

public class Program3 {
    public static void main(String... args) {
        double[] values = {123.5, 456, 243, -33.3};
        ArrayAlg.Pair p = ArrayAlg.minmax(values);

        System.out.println("Min value: " + p.getMin());
        System.out.println("Max value: " + p.getMax());
    }
}
