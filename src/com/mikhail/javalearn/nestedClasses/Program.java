package com.mikhail.javalearn.nestedClasses;

import javax.swing.*;

public class Program {
    public static void main(String... args) {
        var clock = new TalkingClock(1000, true);
        clock.start();

        // Show dialog
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }
}
