package com.mikhail.javalearn.nestedClasses;

import com.mikhail.javalearn.equals.Employee;

public class Program2 {
    public static void main(String... args){

        Employee emp = new Employee("Misha", 123);
        // This is an object of the inner class that extends class Employee
        Employee emp1 = new Employee("Misha", 123) { };

        System.out.println(emp.equals(emp1)); // false, because they related to the different classes!
    }
}
