package com.mikhail.javalearn.nestedClasses;

// It's bad idea, because someone can create a class with the same name (name Pair is the mostly used name),
// but for example, the created class will operate string values
public class Pair {
    private final double max;
    private final double min;

    public Pair(double min, double max) {
        this.min = min;
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }
}
