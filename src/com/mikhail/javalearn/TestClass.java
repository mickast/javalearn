package com.mikhail.javalearn;

public class TestClass {
    // this property should be visible for each class in the current package
    static int code;

    // this property should be visible for each class!
    static public int publicCode;

    static {
        code = 153;
        publicCode = 555;
    }
}
