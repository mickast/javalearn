package com.mikhail.javalearn.params;

public class Program {
    public static void main(String... args) {
        double largestVal = max(1.3, -1.2, 1.9, -5.4, 5.4);
        System.out.println(largestVal);
    }
    private static double max(double... values) {
        double largest = Double.MIN_VALUE;
        for (double v : values) if (v > largest) largest = v;

        return largest;
    }

}
