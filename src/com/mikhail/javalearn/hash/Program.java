package com.mikhail.javalearn.hash;

import com.mikhail.javalearn.equals.Employee;
import com.mikhail.javalearn.equals.Manager;

public class Program {
    public static void main(String[] args) {
        // This strings have different hash codes because String uses it's own hashCode() method
        // but StringBuilder doesn't have it's own hashCode() method and it uses Object's class hashCode() method
        // (calculates on the memory address of the object)
        String s = "ok";
        StringBuilder sb = new StringBuilder("ok");
        System.out.println(s.hashCode() + " " + sb.hashCode());

        String s1 = new String("ok");
        System.out.println(s.hashCode() + " " + s1.hashCode()); // the same

        Employee emp = new Employee("Ivan", 3000);
        System.out.println(emp.hashCode()); // it's own hash code method
        System.out.println(emp.hashCode("code")); // it's own hash code method

        Manager mng = new Manager("Slava", 1000, 400);
        System.out.println(mng.hashCode()); // it's own hash code method
        System.out.println(mng.hashCode("code")); // it's own hash code method

        Manager mng1 = new Manager("Slava", 1000, 400);
        System.out.println(mng1.hashCode()); // it's own hash code method
        System.out.println(mng1.hashCode("code")); // it's own hash code method

    }
}
