package com.mikhail.javalearn.objectShell;

import java.util.ArrayList;

public class Program {
    public static void main(String[] args) {
        var list = new ArrayList<Integer>();
        list.add(3); // the same as previous. Name - auto packing
        list.add(Integer.valueOf(3));

        System.out.println(list.toString());

        int val = list.get(0); // auto unpacking
        int val1 = list.get(0).intValue(); // the same
        System.out.println(val + " " + val1);

        // In this case first of all auto unpacking performs after that we increment the value and then auto packing performs
        Integer n = 3;
        System.out.println(++n);
    }
}
