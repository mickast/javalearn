package com.mikhail.javalearn.lambda;

import java.util.ArrayList;

public class Program3 {
    public static void main(String... args) {

        var evenIsNotAllowed = true;

        var arr = makeArray(10, (i) -> {
            System.out.println("Check i = " + i);
            return i < 5 || (evenIsNotAllowed && i%2 == 0);
        });

        var arr2 = makeArray(10, (i) -> {
            System.out.println("Check i = " + i);
            return i > 5 ;
        });

        System.out.println(arr.toString());
    }

    public static ArrayList<Integer> makeArray(int n, Excludable excludable) {
        var arr = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) if (!excludable.exclude(i)) arr.add(i);
        return arr;
    }
}
