package com.mikhail.javalearn.lambda;

import com.mikhail.javalearn.equals.Employee;

import javax.swing.Timer;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Program {
    public static void main(String... args) {
        String[] sts = new String[]{"Robert", "Shon", "Martin"};

        Comparator<String> comp = (String first, String second) -> first.length() - second.length();

        Comparator<String> cmp1 = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.length() - o1.length();
            }
        };

        Arrays.sort(sts, comp);
        System.out.println(Arrays.toString(sts));
        Arrays.sort(sts, cmp1);
        System.out.println(Arrays.toString(sts));

        ArrayList<Integer> ar = new ArrayList<Integer>();
        ar.add(null);
        ar.add(12);
        ar.add(5);

        System.out.println(ar.toString());

        ar.removeIf(Objects::isNull); // Objects::isNull - link to the method

        System.out.println(ar.toString());

        ar.removeIf(e -> {
            return e < 6;
        });

        System.out.println(ar.toString());

        ar.removeIf(new Rule());
        System.out.println(ar.toString());

        // this interface doesn't take anything but receives something
        Supplier<Employee> userFactory = () -> {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter your name: ");
            var name = in.nextLine();
            System.out.println("Enter your salary: ");
            var salary = in.nextDouble();
            return new Employee(name, salary);
        };

        ArrayList<Employee> employees = new ArrayList<Employee>();
        employees.add(userFactory.get());
        employees.add(userFactory.get());
        employees.add(userFactory.get());

        System.out.println(employees.toString());

        //the same
        UserFactory uf = new UserFactory();
        employees.add(uf.get());
        System.out.println(employees.toString());

        // the same
        Supplier<Employee> spl = new Supplier<Employee>(){
            @Override
            public Employee get() {
                Scanner in = new Scanner(System.in);
                System.out.println("Enter your name: ");
                var name = in.nextLine();
                System.out.println("Enter your salary: ");
                var salary = in.nextDouble();
                return new Employee(name, salary);
            }
        };

        employees.add(spl.get());
        System.out.println(employees.toString());
    }
}
