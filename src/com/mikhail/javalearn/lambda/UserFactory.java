package com.mikhail.javalearn.lambda;

import com.mikhail.javalearn.equals.Employee;

import java.util.Scanner;
import java.util.function.Supplier;

public class UserFactory implements Supplier<Employee> {
    @Override
    public Employee get() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your name: ");
        var name = in.nextLine();
        System.out.println("Enter your salary: ");
        var salary = in.nextDouble();
        return new Employee(name, salary);
    }
}
