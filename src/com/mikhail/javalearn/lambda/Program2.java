package com.mikhail.javalearn.lambda;

import javax.swing.*;
import java.util.function.IntConsumer;

public class Program2 {
    public static void main(String... args) {
        repeat(5, () -> System.out.println("Hello World"));
        repeat(10, i -> System.out.print("CountDown" + (9 - i)));

        var timer = new Timer(1000, System.out::println);
        timer.start();
        // Show dialog
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }

    public static void repeat(int n, Runnable action){
        for (int i = 0; i < n; i++) action.run();
    }

    public static void repeat(int n, IntConsumer action){
        for (int i = 0; i < n; i++) action.accept(i);
    }

}
