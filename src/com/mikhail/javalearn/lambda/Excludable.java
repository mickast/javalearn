package com.mikhail.javalearn.lambda;

@FunctionalInterface
public interface Excludable {
    boolean exclude(int value);
}
