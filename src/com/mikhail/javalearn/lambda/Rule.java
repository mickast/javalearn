package com.mikhail.javalearn.lambda;

import java.util.function.Predicate;

public class Rule implements Predicate<Integer> {
    @Override
    public boolean test(Integer integer) {
        return integer == null || integer < 13;
    }
}
