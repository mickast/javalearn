package com.mikhail.javalearn.enums;

import java.util.Arrays;

public class Program {
    public static void main(String... args) {
        var mediumSize = Size.MEDIUM.toString();
        System.out.println(mediumSize);

        // Get all enums
        Size[] size = Size.values();
        System.out.println(Arrays.toString(size));

        for (Size s : size) {
            System.out.println(s + " has abbreviation " + s.getAbbreviation() + " and has index " + s.ordinal());
        }

        Size s = Size.MEDIUM;

        switch (s) {
            case MEDIUM -> System.out.println("MM");
            case SMALL -> System.out.println("SS");
        }

        // valueOf returns constant that the string corresponds to
        Size sz = Size.valueOf("MEDIUM");
        System.out.println(sz);
    }
}
