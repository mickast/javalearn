package com.mikhail.javalearn.enums;

public enum Size {
    SMALL("S"), MEDIUM("M"); // private static final constants

    private final String abbreviation;

    private Size(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

}
