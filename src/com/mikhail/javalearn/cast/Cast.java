package com.mikhail.javalearn.cast;

import company.CppDeveloper;
import company.Developer;

public class Cast {
    public static void main(String[] args) {
        Employee emp = new Employee();
        emp.setName("Ivan");
        emp.setSalary(123);

        Manager mng = new Manager();
        mng.setName("Grig");
        mng.setSalary(123);
        mng.setBonus(5);

        System.out.println("Ivan's salary: " + emp.getSalary());
        System.out.println("Grig's salary: " + mng.getSalary());

        var newEmp = (Employee) mng;
        System.out.println("New employee can't have any new bonus but he contains previously calculated salary: " + newEmp.getSalary());

        Developer dev = new Developer("Igor", 123, "Ig");
        CppDeveloper cppDev = new CppDeveloper("Sasha", 444, "Sa");
        cppDev.whoAmI();

        Developer newDev = cppDev;
        newDev.whoAmI();
    }
}
