package com.mikhail.javalearn.school;

public class Program {
    public static void main(String... args) {
        Application app = Application.getInstance();
        app.init();
        app.start();
    }
}
