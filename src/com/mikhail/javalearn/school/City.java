package com.mikhail.javalearn.school;

import com.mikhail.javalearn.school.educationalEstablishment.School;

import java.util.ArrayList;

public class City {
    private final String name;
    private ArrayList<School> schools;

    public City(String name) {
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setSchools(ArrayList<School> schools) {
        this.schools = schools;
    }

    public ArrayList<School> getSchools() {
        return schools;
    }
}
