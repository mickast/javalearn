package com.mikhail.javalearn.school;


import com.mikhail.javalearn.school.educationalEstablishment.*;
import com.mikhail.javalearn.school.educationalEstablishment.division.Group;
import com.mikhail.javalearn.school.educationalEstablishment.division.GroupFactory;
import com.mikhail.javalearn.school.educationalEstablishment.division.SchoolGroupFactory;
import com.mikhail.javalearn.school.educationalEstablishment.division.UniversityGroupFactory;
import com.mikhail.javalearn.school.staff.*;
import org.w3c.dom.Attr;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Application {
    private static Application instance;

    private Application() {
    }

    public static Application getInstance() {
        if (instance == null) {
            return new Application();
        }
        return instance;
    }

    public void init() {
        var sgf = new SchoolGroupFactory();
        var sg = sgf.createGroup();
        System.out.println(sg.toString());
    }

    public void start() {

    }


    private EducationalStaffFactory getFactory(Attribute attr) {
        return null;
    }

    private Object[] getRandomSet(Object[] objects) {
        Collections.shuffle(Arrays.asList(objects));
        return objects;
    }

    private ArrayList<City> generateCities(int count) {
        var possibleCityNames = getRandomSet(Attribute.CITIES.getList());

        var cities = new ArrayList<City>();
        for (int i = 0; i < count; i++) {
            cities.add(new City((String) possibleCityNames[i]));
        }

        return cities;
    }

    private ArrayList<EducationalInstitution> generateEducationalInstitution(int count) {
        var educationalInstitutionType = Attribute.SCHOOLS;
        var educationalInstitutions = new ArrayList<EducationalInstitution>();
        var factory = chooseEducationalInstitutionFactory(educationalInstitutionType);
        var possibleEducationalInstitutionNames = getRandomSet(educationalInstitutionType.getList());
        for (int i = 0; i < count; i++) {
            var name = (String) possibleEducationalInstitutionNames[i];
            var educationalInstitution = factory.createEducationalInstitution(name);
            educationalInstitutions.add(educationalInstitution);
        }
        return educationalInstitutions;
    }

    private ArrayList<Group> generateGroups(int count) {
        var groupType = Attribute.UNIVERSITY_GROUPS;
        var factory = chooseGroupFactory(groupType);
        var possibleGroupNames = getRandomSet(groupType.getList());
        var groups = new ArrayList<Group>();
        for (int i = 0; i < count; i++) {
            var name = (String) possibleGroupNames[i];
            var group = factory.createGroup(name);
            groups.add(group);
        }
        return groups;
    }

    private ArrayList<Person> generateEducationalStaff(int count) { //TODO: add gender attr
        var persons = new ArrayList<Person>();
        var personFactoryType = Attribute.SCHOOLS;
        var possibleManNames = getRandomSet(Attribute.MAN_NAMES.getList());
        var possibleManSurnames = getRandomSet(Attribute.MAN_SURNAMES.getList());
        var possibleManPatronymic = getRandomSet(Attribute.MAN_PATRONYMIC.getList());
        var possibleAges = getRandomSet(Attribute.AGES.getList()); //TODO: add lambda

        var factory = chooseEducationalStaffFactory(personFactoryType);
        for (int i = 0; i < count; i++) {
            var name = (String) possibleManNames[i];
            var surname = (String) possibleManSurnames[i];
            var patronymic = (String) possibleManPatronymic[i];
            int age = (int) possibleAges[i];
            var person = factory.createStudent(name, surname, patronymic, age, (Gender) Attribute.GENDERS.getList()[0]); //TODO: make it random and add Teacher
        }
    }

    private EducationalStaffFactory chooseEducationalStaffFactory(Attribute attr) throws IllegalStateException {
        EducationalStaffFactory esf;
        switch (attr){
            case SCHOOLS -> esf = new SchoolStaffFactory();
            case UNIVERSITIES -> esf = new UniversityStaffFactory();
            default -> throw new IllegalStateException("Unexpected value: " + attr);
        }
        return esf;
    }

    private EducationalInstitutionFactory chooseEducationalInstitutionFactory(Attribute attr) throws IllegalStateException {
        EducationalInstitutionFactory eif;
        switch (attr) {
            case SCHOOLS -> eif = new SchoolFactory();
            case UNIVERSITIES -> eif = new UniversityFactory();
            default -> throw new IllegalStateException("Unexpected value: " + attr);
        }
        return eif;
    }

    private GroupFactory chooseGroupFactory(Attribute attr) throws IllegalStateException {
        GroupFactory gf;
        switch (attr) {
            case UNIVERSITY_GROUPS -> gf = new UniversityGroupFactory();
            case SCHOOL_GROUPS -> gf = new SchoolGroupFactory();
            default -> throw new IllegalStateException("Unexpected value: " + attr);
        }
        return gf;
    }
}
