package com.mikhail.javalearn.school;

import com.mikhail.javalearn.school.staff.Gender;

enum Attribute {
    //TODO: how can I realize getting man's and woman's names separately?
    MAN_NAMES {
        public String[] getList() {
            return new String[] {
                    "Ivan", "Lev", "Victor", "Stepan", "Karl", "Gleb",
                    "Maksim", "Mikhail", "Denis", "Anton", "Petr", "Pavel", "Stanislav",
                    "Ignat", "Konstantin", "Leonid", "Evgeny", "Ilya", "Dmirty"
            };
        };
    },
    MAN_SURNAMES {
        public String[] getList() {
            return new String[] {
                    "Kopylov", "Ivanov", "Petrov", "Chernov", "Kokuisky", "Babenin",
                    "Fedotov", "Krasnov", "Salnokov", "Permoniov", "Alexeev", "Knyazev", "Kuznetsov",
                    "Aptrahmanov", "Akimov", "Tolmachev", "Yartsev", "Nikolaev", "Krivilev"
            };
        };
    },
    MAN_PATRONYMIC {
        public String[] getList() {
            return new String[] {
                    "Ivanovich", "Vacheslavovich", "Sergeevich", "Valerievich", "Danilivich", "Petrovich",
                    "Olegovich", "Anatolievich", "Efimovich", "Stepanovich", "Artemievich", "Denisovich", "Nikolaevich",
                    "Maksimovich", "Leonidovich", "Yakovlevich", "Leonidovich", "Karlovich", "Inokentievich"
            };
        };
    },
    WOMAN_NAMES {
        public String[] getList() {
            return new String[] {
                    "Elena", "Irina", "Ekaterina", "Evgenya", "Eva",
                    "Ksenia", "Maria", "Daria", "Svetlana", "Juia",
                    "Sophia", "Klara", "Kristina", "Olga", "Karina"
            };
        };
    },
    WOMAN_SURNAMES {
        public String[] getList() {
            return new String[] {
                    "Kopylova", "Demidova", "Patrina", "Galatova", "Kislitsina",
                    "Vahrusheva", "Petrova", "Demyanova", "Sergeeva", "Sobina",
                    "Krasnova", "Efimova", "Dementieva", "Fedotova", "Ermolaeva"
            };
        };
    },
    WOMAN_PATRONYMIC {
        public String[] getList() {
            return new String[] {
                    "Efimovna", "Denisovna", "Olegovna", "Stepanovna", "Semenovna",
                    "Karlovna", "Petrovna", "Mihaylovna", "Sergeevna", "Konstantinovna",
                    "Dmitrievna", "Leonidovna", "Antonovna", "Igorevna", "Artemovna"
            };
        };
    },
    GENDERS {
        public Gender[] getList() {
            return Gender.values();
        }
    },
    AGES {
        public int[] getList() {
            return new int[]{5,10,15,16,17,18,19,20,21,22,23,24,32,25,27,67,45,36,47,61};
        };
    },
    UNIVERSITIES {
        public String[] getList() {
            return new String[]{"ISTU", "IGMA", "MGU", "PNIPU"};
        }
    },
    SCHOOLS {
        @Override
        public Object[] getList() {
            return new Object[0];
        }

        public String[] getSchoolList() {
            return new String[]{"School#1", "School#", "School#", "School#", "School#5", "School#6", "School#7"};
        }
    },
    CITIES {
        public String[] getList() {
            return new String[]{"Izhevsk", "Moskva", "Saint Petersburg", "Ekaterinburg", "Omsk", "Saratov"};
        }
    },
    UNIVERSITY_GROUPS {
        public String[] getList() {
            return new String[]{"B01", "B02", "B03", "B04", "B05", "B06"};
        }
    },
    SCHOOL_GROUPS {
        public String[] getList() {
            return new String[]{"1A", "1B", "1C", "2A", "2B", "2C"};
        }
    };
    public abstract Object[] getList();
}
