package com.mikhail.javalearn.school.staff;

public enum Gender {

    MALE('m'), FEMALE('f');

    private final char genderCode;

    private Gender(char genderCode) {
        this.genderCode = genderCode;
    }

    public char getGenderCode() {
        return genderCode;
    }
}
