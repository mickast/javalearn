package com.mikhail.javalearn.school.staff;

public interface EducationalStaffFactory {
    Student createStudent(String name, String surname, String patronymic, int age, Gender gender);
    Teacher createTeacher(String name, String surname, String patronymic, int age, Gender gender);
}
