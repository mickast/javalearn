package com.mikhail.javalearn.school.staff;

public abstract class Person {
    private final String name;
    private final int age;
    private final Gender gender;

    public Person(String name, String surname, String patronymic, int age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return  age;
    }

    public Gender getGender() {
        return gender;
    }
}
