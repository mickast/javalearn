package com.mikhail.javalearn.school.staff;

public class SchoolStaffFactory implements EducationalStaffFactory {
    @Override
    public Student createStudent() {
        return new Student("Ivan", "Ivanov", "Ivanovich", 15, Gender.MALE);
    }

    @Override
    public Teacher createTeacher() {
        return new Teacher("Galina", "Semenova", "Pavlovna", 40, Gender.FEMALE);
    }

}
