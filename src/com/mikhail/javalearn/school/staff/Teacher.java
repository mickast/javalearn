package com.mikhail.javalearn.school.staff;


import com.mikhail.javalearn.school.Subject;

import java.util.ArrayList;

public class Teacher extends Person {
    private ArrayList<Subject> subjects;
    private ArrayList<Student> students;

    public Teacher(String name, String surname, String patronymic, int age, Gender gender) {
        super(name, surname, patronymic, age, gender);
    }

    public void setSubjects(ArrayList<Subject> subjects) {
        this.subjects = subjects;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<Subject> getSubjects() {
        return subjects;
    }


    public ArrayList<Student> getStudents() {
        return students;
    }
}
