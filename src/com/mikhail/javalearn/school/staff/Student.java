package com.mikhail.javalearn.school.staff;

public class Student extends Person {
    public Student(String name, String surname, String patronymic, int age, Gender gender) {
        super(name, surname, patronymic, age, gender);
    }
}
