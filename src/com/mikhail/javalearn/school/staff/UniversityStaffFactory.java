package com.mikhail.javalearn.school.staff;

public class UniversityStaffFactory implements EducationalStaffFactory {
    @Override
    public Student createStudent() {
        return new Student("Georgii", "Stepnov", "Petrovich", 21, Gender.MALE);
    }

    @Override
    public Teacher createTeacher() {
        return new Teacher("Valeriya", "Perevozchikiva", "Dmitrievna", 37, Gender.FEMALE);
    }
}
