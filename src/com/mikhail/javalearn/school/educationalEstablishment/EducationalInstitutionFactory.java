package com.mikhail.javalearn.school.educationalEstablishment;

public interface EducationalInstitutionFactory {
    EducationalInstitution createEducationalInstitution(String name);
}
