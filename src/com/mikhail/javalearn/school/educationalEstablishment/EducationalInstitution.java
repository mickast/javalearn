package com.mikhail.javalearn.school.educationalEstablishment;

import com.mikhail.javalearn.school.educationalEstablishment.division.Group;
import com.mikhail.javalearn.school.staff.Teacher;

import java.util.ArrayList;

public abstract class EducationalInstitution {
    private final String name;
    private ArrayList<Group> groups;
    private ArrayList<Teacher> teachers;

    public EducationalInstitution(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setTeachers(ArrayList<Teacher> teachers) {
        this.teachers = teachers;
    }

    public ArrayList<Teacher> getTeachers() {
        return teachers;
    }
}
