package com.mikhail.javalearn.school.educationalEstablishment;

public class UniversityFactory implements EducationalInstitutionFactory{
    @Override
    public University createEducationalInstitution(String name) {
        return new University(name);
    }
}
