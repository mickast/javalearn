package com.mikhail.javalearn.school.educationalEstablishment;

public class SchoolFactory implements EducationalInstitutionFactory {
    @Override
    public School createEducationalInstitution(String name) {
        return new School(name);
    }
}
