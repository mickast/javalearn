package com.mikhail.javalearn.school.educationalEstablishment.division;

public class UniversityGroupFactory implements GroupFactory{
    @Override
    public UniversityGroup createGroup(String name) {
        return new UniversityGroup(name);
    }
}
