package com.mikhail.javalearn.school.educationalEstablishment.division;

public interface GroupFactory {
    Group createGroup(String name);
}
