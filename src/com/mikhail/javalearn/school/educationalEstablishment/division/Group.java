package com.mikhail.javalearn.school.educationalEstablishment.division;

import com.mikhail.javalearn.school.staff.Student;

import java.util.ArrayList;

public abstract class Group {
    private final String name;
    private ArrayList<Student> students;

    public Group(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }
}
