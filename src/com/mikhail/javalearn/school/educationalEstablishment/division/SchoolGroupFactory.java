package com.mikhail.javalearn.school.educationalEstablishment.division;

public class SchoolGroupFactory implements GroupFactory {
    @Override
    public SchoolGroup createGroup(String name) {
        return new SchoolGroup(name);
    }
}
