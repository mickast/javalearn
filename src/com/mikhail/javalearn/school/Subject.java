package com.mikhail.javalearn.school;

public enum Subject {

    MATHEMATICS("Maths"), HISTORY("History"), PHYSICS("Physics"), GEOGRAPHY("Geography"),
    INFORMATICS("Informatics"), BIOLOGY("Biology"), GEOMETRY("Geometry"),
    LITERATURE("Literature"), MUSIC("Music"), CHEMISTRY("Chemistry"), DRAWING("Drawing");

    private final String name;

    private Subject(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}