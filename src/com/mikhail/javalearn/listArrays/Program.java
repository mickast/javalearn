package com.mikhail.javalearn.listArrays;

import com.mikhail.javalearn.equals.Employee;

import static java.lang.System.*;
import java.util.ArrayList;

public class Program {
    public static void main(String[] args) {

        // We can add to the ArrayList with Object type elements with any type
        var arr = new ArrayList<>();
        arr.add(1);
        arr.add("fgh");
        out.println(arr.toString());

        // ArrayList type is Object
        var obj = new ArrayList<>();

        // the same
        ArrayList<Employee> staff = new ArrayList<Employee>();
        // define capacity of the ArrayList
        staff.ensureCapacity(5);
        staff.add(new Employee("Ignat", 2000));
        staff.add(new Employee("Sasha", 5000));

        // trim ArrayList capacity to current count of the ArrayList's elements (saving memory)
        staff.trimToSize();
        out.println("Element count: " + staff.size());

        // Update ArrayList element by index
        staff.set(0, new Employee("new Ignat", 2000));
        out.println(staff.toString());

        // get element of the ArrayList by index
        var person = staff.get(1);
        out.println(person.toString());

        // remove element by index
        staff.remove(1);
        out.println(staff.toString());

        for (Employee e : staff) {
            out.println(e.toString());
        }


    }
}
