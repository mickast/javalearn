package com.mikhail.javalearn.abs;

import static java.lang.System.out;

public class Program {
    public static void main(String[] args) {
        Person stud = new Student(1); //This is an objective variable of th abstract class
        stud.setName("Ilya");
        stud.setHobby("Tennis");
        stud.setDescription("Cool man!");

        out.println("Hello!");
        out.println("My name is " + stud.getName());
        out.println("My number is " + stud.getNumber());
        out.println("My favourite hobby is " + stud.getHobby());
        out.println("I'm a " + stud.getDescription());
    }
}
