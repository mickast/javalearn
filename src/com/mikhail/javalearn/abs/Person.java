package com.mikhail.javalearn.abs;

abstract public class Person {
    private String name;
    private String hobby;
    private int number;

    public Person(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public int getNumber(){
        return this.number;
    }

    abstract public void setDescription(String description);

    abstract public String getDescription();
}
