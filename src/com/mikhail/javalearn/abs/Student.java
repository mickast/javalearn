package com.mikhail.javalearn.abs;

public class Student extends Person {
    private String description;

    public Student(int number) {
        super(number);
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }
}
