package com.mikhail.javalearn;

import com.mikhail.javalearn.internal.InternalClass;

public class BasePackageClass {
    public static void main(String[] args) {
        int code = TestClass.code; // This property should be visible around the current package!(not in the nested packages)
        int publicCode = TestClass.publicCode; // This property we can see everywhere
        System.out.println("Protected code (visible only in the current package): " + code);
        System.out.println("Public code (visible everywhere): " + publicCode);

        var intern = new InternalClass();
        intern.printCodeFromTheTestClass();
    }
}
