package com.mikhail.javalearn.tostr;

import com.mikhail.javalearn.equals.Employee;
import com.mikhail.javalearn.equals.Manager;

import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        var employee = new Employee("Ignat", 4000);
        System.out.println(employee.toString());

        var manager = new Manager("David", 6000, 300);
        System.out.println(manager.toString());

        int[] numbers = {1 ,2 ,3 ,4 ,5};
        // write [I@6f539caf, because arrays in java extends Object class
        System.out.println(numbers.toString());

        // we can call toString method of the Arrays class
        System.out.println(Arrays.toString(numbers)); // [1, 2, 3, 4, 5]
    }
}
