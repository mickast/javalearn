package com.mikhail.javalearn.comparator;

import java.util.Arrays;

public class Program {
    public static void main(String... args) {
        String[] friends = {"RRR", "RR", "R", "RRRRR"};
        var lc = new LengthComparator();

        System.out.println(lc.compare(friends[0], friends[2]));

        Arrays.sort(friends, lc);

        System.out.println(Arrays.toString(friends));
    }
}
