package com.mikhail.javalearn.exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class Resource {
    public static ArrayList<String> readFileWithTryResource() {
        ArrayList<String> strArr = new ArrayList<>();

        try(var in = new Scanner(
                new FileInputStream("/home/mikhail/projects/src/com/mikhail/javalearn/exceptions/tst.txt"),
                StandardCharsets.UTF_8
        )) {
            while (in.hasNext()) {
                strArr.add(in.next().toUpperCase());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Exception: " + e.getMessage() + " " + e.getClass() + " " + e.getCause());
        } // There in no need to write finally block because

        return strArr;
    }

    public static ArrayList<String> readFile() {
        ArrayList<String> strArr = new ArrayList<>();

        Scanner in = null;
        try {
            in = new Scanner(new FileInputStream("/home/mikhail/projects/src/com/mikhail/javalearn/exceptions/test.txt"), StandardCharsets.UTF_8);
            while (in.hasNext()) {
                strArr.add(in.next().toUpperCase());
            }
        } catch (IOException e) {
            System.out.println("Exception: " + e.getMessage() + " " + e.getClass() + " " + e.getCause());
        } finally {
            try {
                if (in != null) in.close();
            } catch (NullPointerException e) {
                System.out.println("Exception: " + e.getMessage() + " " + e.getClass() + " " + e.getCause());
            }
        }

        return strArr;
    }
}
