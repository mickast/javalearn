package com.mikhail.javalearn.callback;

import javax.swing.*;

public class Program {
    public static void main(String... args) {
        // Create listener to perform an action when some event will be called
        var listener = new TimePrinter();

        // Create a timer. Every 1000ms Timer will call actionPerformed() method from the listener
        Timer tm = new Timer(1000, listener);
        tm.start(); // Start timer

        // Show dialog
        JOptionPane.showMessageDialog(null, "Quit program?");
        System.exit(0);
    }
}
