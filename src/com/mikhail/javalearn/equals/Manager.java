package com.mikhail.javalearn.equals;

public class Manager extends Employee {
    final private double bonus;

    public Manager(String name, double salary, double bonus) {
        super(name, salary);
        this.bonus = bonus;
    }

    @Override
    public boolean equals(Object otherObject) {
        // initialize parent equals method to perform base checking for name, salary, null and others
        if (!super.equals(otherObject)) return false;

        // Now we know that otherObject related to the current class(previous method does this checking)
        // perform bonus comparison
        if (bonus != ((Manager) otherObject).bonus) {
            System.out.println("Both objects " + this.getClass() + ", " + otherObject.getClass() + " have different bonus.");
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + Double.hashCode(bonus);
    }

    @Override
    public int hashCode(String string) {
        return super.hashCode(string) + Double.hashCode(bonus);
    }

    @Override
    public String toString() {
        return getClass().getName()
                + "[name: " + super.getName() + ","
                + "salary: " + super.getSalary() + ""
                + "bonus: " + bonus + "" +
                "]";
    }
}
