package com.mikhail.javalearn.equals;

import java.util.Objects;

public class Employee implements Comparable<Employee>{
    final private String name;
    final private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object otherObject){
        // fast checking objects for identity
        if (this == otherObject) {
            System.out.println("Both objects " + this.getClass() + ", " + otherObject.getClass() + " are the same.");
            return true;
        }

        // checking if otherObject is null
        if (otherObject == null) {
            System.out.println("Object is null");
            return false;
        }

        // if classes is not match than they are not equal
        if (getClass() != otherObject.getClass()) {
            System.out.println("objects " + this.getClass() + ", " + otherObject.getClass() + " related to different classes.");
            return false;
        }

        Employee other = (Employee) otherObject;

        if (!name.equals(other.name) || salary != other.salary) {
            System.out.println("objects " + this.getClass() + ", " + otherObject.getClass() + " have different names or salaries.");
            return false;
        }

        return true;
    }

    @Override
    public int hashCode(){
        return Objects.hash(name, salary);
    }

    public int hashCode(String string){
        return string.hashCode() + name.hashCode() + Double.hashCode(salary);
    }

    @Override
    public String toString() {
        return getClass().getName()
                + "[name: " + name + ","
                + "salary: " + salary + "" +
                "]";
    }

    // this method allows us to know which object has a biggest value
    // -1 - passed object's value is bigger; 0 - equal; 1 - passed object's value is smaller;
    public int compareTo(Employee otherEmployee) {
        // If we realize not generic Comparable interface than we should cast object as it mentioned above
        //Employee other = (Employee) otherObject;
        //return Double.compare(salary, other.salary);
        return Double.compare(salary, otherEmployee.salary);
    }
}
