package com.mikhail.javalearn.equals;

import static java.lang.System.*;

public class Program {
    public static void main(String[] args) {
        // equals is true when both variables linked to the same object (default equals checking)
        var manager = new Manager("Ivan", 5_000, 300);
        var manager1 = new Manager("Divan", 3000, 100);
        var manager2 = new Manager("Ivan", 5_000, 300);
        var manager3 = new Manager("Ivan", 5_000, 200);

        var employee = new Employee("Grig", 2000);

        out.println(manager.equals(manager));
        out.println(manager.equals(manager1));
        out.println(manager.equals(manager2));
        out.println(manager.equals(manager3));

        out.println(employee.equals(manager));
    }
}
