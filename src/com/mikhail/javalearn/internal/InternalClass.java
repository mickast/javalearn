package com.mikhail.javalearn.internal;

import com.mikhail.javalearn.TestClass;

public class InternalClass {
    public void printCodeFromTheTestClass() {
        var publicCode = TestClass.publicCode;
        System.out.println("Here we get publicCode from the InternalClass: " + publicCode);
    }
}
