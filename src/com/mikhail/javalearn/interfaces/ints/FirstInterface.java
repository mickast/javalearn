package com.mikhail.javalearn.interfaces.ints;

public interface FirstInterface {
    default int getCode() { return 0; }
}
