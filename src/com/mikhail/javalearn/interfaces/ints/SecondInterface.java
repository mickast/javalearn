package com.mikhail.javalearn.interfaces.ints;

public interface SecondInterface {
    default int getCode() { return 1; }
}
