package com.mikhail.javalearn.interfaces;

import com.mikhail.javalearn.equals.Employee;

import java.util.Arrays;

public class Program {
    public static void main(String... args) {
        var employee1 = new Employee("Robert", 3000);
        var employee2 = new Employee("Stas", 5000);
        var employee3 = new Employee("Shon", 5000);
        var employee4 = new Employee("Shon", 1000);


        System.out.println(employee1.compareTo(employee2));
        System.out.println(employee2.compareTo(employee1));
        System.out.println(employee2.compareTo(employee3));

        Employee[] arr = {employee2, employee1, employee3, employee4};

        Arrays.sort(arr);

        System.out.println(Arrays.toString(arr));

        var tc = new TestClass();
        System.out.println(tc.getCode());

    }
}
