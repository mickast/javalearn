package com.mikhail.javalearn.interfaces;

import com.mikhail.javalearn.interfaces.ints.*;

public class TestClass  implements FirstInterface, SecondInterface {
    // This is a first way to resolve this occasion. In this case default realizations will be ignored
//    public int getCode() {
//        return 50;
//    }

    // To resolve this kind of conflict we can call a method from the needed interface
    @Override
    public int getCode() {
        return FirstInterface.super.getCode();
    }
}
