package com.mikhail.javalearn.stackTrace;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Program {
    public static void main(String... args) {
        var t = new Throwable();
        var out = new StringWriter();
        t.printStackTrace(new PrintWriter(out));

        StackWalker sw = StackWalker.getInstance();
        sw.forEach(stackFrame -> {
            System.out.println(stackFrame);
        });
    }
}
