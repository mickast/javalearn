package com.company;
import languageConstructions.ComparingStrings;
import ClassesAndObjects.Console;
import ClassesAndObjects.User;
import ClassesAndObjects.UserView;

public class Main {

    public static void main(String[] args) {
      //Chapt01 ch = new Chapt01();
      //ch.scanAndOutput();
        System.out.println("");
        System.out.println("******* Classes and objects *******");
        User user; // Link announcement
        user = new User(); // Object creation

        user.setNumericCode(45);
        user.setPassword("testpass");

        UserView.welcome(user);

        System.out.println("");
        System.out.println("******* Comparing strings *******");

        System.out.println("");
        System.out.println("******* Console *******");
        Console.readChar();
        Console.readName();
    }
}
