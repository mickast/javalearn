import company.Developer;
import static java.lang.System.*;

// src - base catalog

// This class is located in the base catalog in the unnamed package. So we can compile it by the following command: javac BaseCatalogPackageClass.java
// after that Developer class will be found automatically
// Now we need to run the program(interpret): java BaseCatalogPackageClass in the same directory/
public class BaseCatalogPackageClass {
    public static void main(String[] args) {
        Developer dev = new Developer("Igor", 234d, "testn");
        out.println("IGOR!!!!");
    }
}
