package languageConstructions;

public class ComparingStrings {
    public static void main(String[] args) {
        String s1, s2;
        s1 = "Java";
        s2 = s1; // the variable refers to the same string
        System.out.println("link comparison " + (s1 == s2)); //true

        //Create a new object by adding a new symbol
        s1 += '2';
        s2 = new String(s1);
        System.out.println("link comparison " + (s1 == s2)); //false

        System.out.println("value comparison " + (s1.equals(s2))); //true
        {
            int y = 0;
        }
    }
}
