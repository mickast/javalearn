package languageConstructions;

public class Constants {
    // constant definition. Private by default
    static final double CM_PER_INCH = 2.54;

    public static void main(String[] agrs) {
        // constant definition
        //final double CM_PER_INCH = 2.54;
        double paperWidth = 8.5;
        double paperHeight = 11;

        System.out.println("Paper size in centimeters: " + paperWidth * CM_PER_INCH + "by" + paperHeight * CM_PER_INCH);
    }
}