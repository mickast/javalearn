package languageConstructions;

public class Types {
    public void printTypes() {
        // integer types
        int iNum = 58756875; // 4 byte, > +- 2 mlrd
        int isNum = 1_000_000;

        short sNum = -32768; // 2 byte, from -32768 to 32767

        long lNum = 234234; // 8 byte, from -9223372036854775808 to 9223372036854775807
        long blNum = 4000000000L;

        // Floating point types
        float fNum = 3.14f; // 4 byte
        double dNum = 3.142345234234; // 8 byte (this type has double accuracy in comparison with float type)

        double dpiNum = Double.POSITIVE_INFINITY;
        double dniNum = Double.NEGATIVE_INFINITY;
        double dnNum = Double.NaN;

        // symbolic type
        char cNum = 'A';
        char ctmNum = '\u2122'; // TM

        // logical type
        boolean bNum = true;

        // String type
        String str = "str";
        String newStr = new String("str");
        String subStr = newStr.substring(0, 3);
        String all = String.join("/","part1", "part2");
    }
}
